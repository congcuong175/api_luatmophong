﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class BAILAMsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/BAILAMs
        public IHttpActionResult GetBAILAMs()
        {
            return Ok(new { status = true, data = db.BAILAMs });
        }

        // GET: api/BAILAMs/5
        [ResponseType(typeof(BAILAM))]
        public IHttpActionResult GetBAILAM(int id)
        {
            BAILAM bAILAM = db.BAILAMs.Find(id);
            if (bAILAM == null)
            {
                return NotFound();
            }

            return Ok(bAILAM);
        }

        // PUT: api/BAILAMs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBAILAM(int id, BAILAM bAILAM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bAILAM.MABAILAM)
            {
                return BadRequest();
            }

            db.Entry(bAILAM).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BAILAMExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BAILAMs
        [ResponseType(typeof(BAILAM))]
        public IHttpActionResult PostBAILAM(BAILAM bAILAM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BAILAMs.Add(bAILAM);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BAILAMExists(bAILAM.MABAILAM))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = bAILAM.MABAILAM }, bAILAM);
        }

        // DELETE: api/BAILAMs/5
        [ResponseType(typeof(BAILAM))]
        public IHttpActionResult DeleteBAILAM(int id)
        {
            BAILAM bAILAM = db.BAILAMs.Find(id);
            if (bAILAM == null)
            {
                return NotFound();
            }

            db.BAILAMs.Remove(bAILAM);
            db.SaveChanges();

            return Ok(bAILAM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BAILAMExists(int id)
        {
            return db.BAILAMs.Count(e => e.MABAILAM == id) > 0;
        }
    }
}