﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class CAUHOIsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/CAUHOIs
        public IHttpActionResult GetCAUHOIs()
        {
            return Ok(new {status=true,data=db.CAUHOIs});
        }

        // GET: api/CAUHOIs/5
        [ResponseType(typeof(CAUHOI))]
        public IHttpActionResult GetCAUHOI(int id)
        {
            CAUHOI cAUHOI = db.CAUHOIs.Find(id);
            if (cAUHOI == null)
            {
                return NotFound();
            }

            return Ok(new { status = true, data = cAUHOI });
        }

        // PUT: api/CAUHOIs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCAUHOI(int id, CAUHOI cAUHOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cAUHOI.MACAUHOI)
            {
                return BadRequest();
            }

            db.Entry(cAUHOI).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CAUHOIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CAUHOIs
        [ResponseType(typeof(CAUHOI))]
        public IHttpActionResult PostCAUHOI(CAUHOI cAUHOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAUHOIs.Add(cAUHOI);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CAUHOIExists(cAUHOI.MACAUHOI))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cAUHOI.MACAUHOI }, cAUHOI);
        }

        // DELETE: api/CAUHOIs/5
        [ResponseType(typeof(CAUHOI))]
        public IHttpActionResult DeleteCAUHOI(int id)
        {
            CAUHOI cAUHOI = db.CAUHOIs.Find(id);
            if (cAUHOI == null)
            {
                return NotFound();
            }

            db.CAUHOIs.Remove(cAUHOI);
            db.SaveChanges();

            return Ok(cAUHOI);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CAUHOIExists(int id)
        {
            return db.CAUHOIs.Count(e => e.MACAUHOI == id) > 0;
        }
    }
}