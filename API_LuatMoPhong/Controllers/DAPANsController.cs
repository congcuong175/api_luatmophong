﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class DAPANsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/DAPANs
        public IHttpActionResult GetDAPANs()
        {
            return Ok(new { status = true, data = db.DAPANs });
        }

        // GET: api/DAPANs/5
        [ResponseType(typeof(DAPAN))]
        public IHttpActionResult GetDAPAN(int id)
        {
            DAPAN dAPAN = db.DAPANs.Find(id);
            if (dAPAN == null)
            {
                return NotFound();
            }

            return Ok(dAPAN);
        }

        // PUT: api/DAPANs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDAPAN(int id, DAPAN dAPAN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dAPAN.MADAPAN)
            {
                return BadRequest();
            }

            db.Entry(dAPAN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DAPANExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DAPANs
        [ResponseType(typeof(DAPAN))]
        public IHttpActionResult PostDAPAN(DAPAN dAPAN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DAPANs.Add(dAPAN);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DAPANExists(dAPAN.MADAPAN))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dAPAN.MADAPAN }, dAPAN);
        }

        // DELETE: api/DAPANs/5
        [ResponseType(typeof(DAPAN))]
        public IHttpActionResult DeleteDAPAN(int id)
        {
            DAPAN dAPAN = db.DAPANs.Find(id);
            if (dAPAN == null)
            {
                return NotFound();
            }

            db.DAPANs.Remove(dAPAN);
            db.SaveChanges();

            return Ok(dAPAN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DAPANExists(int id)
        {
            return db.DAPANs.Count(e => e.MADAPAN == id) > 0;
        }
    }
}