﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class DETHIsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/DETHIs
        public IHttpActionResult GetDETHIs()
        {
            return Ok(new { status = true, data = db.DETHIs });
        }

        // GET: api/DETHIs/5
        [ResponseType(typeof(DETHI))]
        public IHttpActionResult GetDETHI(int id)
        {
            DETHI dETHI = db.DETHIs.Find(id);
            if (dETHI == null)
            {
                return NotFound();
            }

            return Ok(dETHI);
        }

        // PUT: api/DETHIs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDETHI(int id, DETHI dETHI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dETHI.MADE)
            {
                return BadRequest();
            }

            db.Entry(dETHI).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DETHIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DETHIs
        [ResponseType(typeof(DETHI))]
        public IHttpActionResult PostDETHI(DETHI dETHI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DETHIs.Add(dETHI);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DETHIExists(dETHI.MADE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dETHI.MADE }, dETHI);
        }

        // DELETE: api/DETHIs/5
        [ResponseType(typeof(DETHI))]
        public IHttpActionResult DeleteDETHI(int id)
        {
            DETHI dETHI = db.DETHIs.Find(id);
            if (dETHI == null)
            {
                return NotFound();
            }

            db.DETHIs.Remove(dETHI);
            db.SaveChanges();

            return Ok(dETHI);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DETHIExists(int id)
        {
            return db.DETHIs.Count(e => e.MADE == id) > 0;
        }
    }
}