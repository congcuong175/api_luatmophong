﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class LAMTHEOLOAIsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/LAMTHEOLOAIs
        public IHttpActionResult GetLAMTHEOLOAIs()
        {
            return Ok(new { status = true, data = db.LAMTHEOLOAIs });
        }

        // GET: api/LAMTHEOLOAIs/5
        [ResponseType(typeof(LAMTHEOLOAI))]
        public IHttpActionResult GetLAMTHEOLOAI(int id)
        {
            LAMTHEOLOAI lAMTHEOLOAI = db.LAMTHEOLOAIs.Find(id);
            if (lAMTHEOLOAI == null)
            {
                return NotFound();
            }

            return Ok(lAMTHEOLOAI);
        }

        // PUT: api/LAMTHEOLOAIs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLAMTHEOLOAI(int id, LAMTHEOLOAI lAMTHEOLOAI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lAMTHEOLOAI.MALAMTHEOLOAI)
            {
                return BadRequest();
            }

            db.Entry(lAMTHEOLOAI).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LAMTHEOLOAIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LAMTHEOLOAIs
        [ResponseType(typeof(LAMTHEOLOAI))]
        public IHttpActionResult PostLAMTHEOLOAI(LAMTHEOLOAI lAMTHEOLOAI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LAMTHEOLOAIs.Add(lAMTHEOLOAI);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (LAMTHEOLOAIExists(lAMTHEOLOAI.MALAMTHEOLOAI))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = lAMTHEOLOAI.MALAMTHEOLOAI }, lAMTHEOLOAI);
        }

        // DELETE: api/LAMTHEOLOAIs/5
        [ResponseType(typeof(LAMTHEOLOAI))]
        public IHttpActionResult DeleteLAMTHEOLOAI(int id)
        {
            LAMTHEOLOAI lAMTHEOLOAI = db.LAMTHEOLOAIs.Find(id);
            if (lAMTHEOLOAI == null)
            {
                return NotFound();
            }

            db.LAMTHEOLOAIs.Remove(lAMTHEOLOAI);
            db.SaveChanges();

            return Ok(lAMTHEOLOAI);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LAMTHEOLOAIExists(int id)
        {
            return db.LAMTHEOLOAIs.Count(e => e.MALAMTHEOLOAI == id) > 0;
        }
    }
}