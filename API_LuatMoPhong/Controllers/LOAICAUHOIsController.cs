﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class LOAICAUHOIsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/LOAICAUHOIs
        public IHttpActionResult GetLOAICAUHOIs()
        {
            
            return Ok(new { status = true, data = db.LOAICAUHOIs });
        }

        // GET: api/LOAICAUHOIs/5
        [ResponseType(typeof(LOAICAUHOI))]
        public IHttpActionResult GetLOAICAUHOI(int id)
        {
            LOAICAUHOI lOAICAUHOI = db.LOAICAUHOIs.Find(id);
            if (lOAICAUHOI == null)
            {
                return NotFound();
            }

            return Ok(lOAICAUHOI);
        }

        // PUT: api/LOAICAUHOIs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLOAICAUHOI(int id, LOAICAUHOI lOAICAUHOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lOAICAUHOI.MALOAI)
            {
                return BadRequest();
            }

            db.Entry(lOAICAUHOI).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LOAICAUHOIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LOAICAUHOIs
        [ResponseType(typeof(LOAICAUHOI))]
        public IHttpActionResult PostLOAICAUHOI(LOAICAUHOI lOAICAUHOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LOAICAUHOIs.Add(lOAICAUHOI);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (LOAICAUHOIExists(lOAICAUHOI.MALOAI))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = lOAICAUHOI.MALOAI }, lOAICAUHOI);
        }

        // DELETE: api/LOAICAUHOIs/5
        [ResponseType(typeof(LOAICAUHOI))]
        public IHttpActionResult DeleteLOAICAUHOI(int id)
        {
            LOAICAUHOI lOAICAUHOI = db.LOAICAUHOIs.Find(id);
            if (lOAICAUHOI == null)
            {
                return NotFound();
            }

            db.LOAICAUHOIs.Remove(lOAICAUHOI);
            db.SaveChanges();

            return Ok(lOAICAUHOI);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LOAICAUHOIExists(int id)
        {
            return db.LOAICAUHOIs.Count(e => e.MALOAI == id) > 0;
        }
    }
}