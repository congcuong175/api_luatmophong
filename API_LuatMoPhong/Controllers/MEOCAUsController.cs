﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_LuatMoPhong.Models;

namespace API_LuatMoPhong.Controllers
{
    public class MEOCAUsController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/MEOCAUs
        public IHttpActionResult GetMEOCAUs()
        {
            return Ok(new { status = true, data = db.MEOCAUs });
        }

        // GET: api/MEOCAUs/5
        [ResponseType(typeof(MEOCAU))]
        public IHttpActionResult GetMEOCAU(int id)
        {
            MEOCAU mEOCAU = db.MEOCAUs.Find(id);
            if (mEOCAU == null)
            {
                return NotFound();
            }

            return Ok(mEOCAU);
        }

        // PUT: api/MEOCAUs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMEOCAU(int id, MEOCAU mEOCAU)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mEOCAU.MAMEO)
            {
                return BadRequest();
            }

            db.Entry(mEOCAU).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MEOCAUExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MEOCAUs
        [ResponseType(typeof(MEOCAU))]
        public IHttpActionResult PostMEOCAU(MEOCAU mEOCAU)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MEOCAUs.Add(mEOCAU);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MEOCAUExists(mEOCAU.MAMEO))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mEOCAU.MAMEO }, mEOCAU);
        }

        // DELETE: api/MEOCAUs/5
        [ResponseType(typeof(MEOCAU))]
        public IHttpActionResult DeleteMEOCAU(int id)
        {
            MEOCAU mEOCAU = db.MEOCAUs.Find(id);
            if (mEOCAU == null)
            {
                return NotFound();
            }

            db.MEOCAUs.Remove(mEOCAU);
            db.SaveChanges();

            return Ok(mEOCAU);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MEOCAUExists(int id)
        {
            return db.MEOCAUs.Count(e => e.MAMEO == id) > 0;
        }
    }
}