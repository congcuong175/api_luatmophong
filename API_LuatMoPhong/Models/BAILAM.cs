namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BAILAM")]
    public partial class BAILAM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MABAILAM { get; set; }

        public int? MADE { get; set; }

        public int? MACAUHOI { get; set; }

        public double? DIEMXN { get; set; }

        public int? KIEMTRA { get; set; }
    }
}
