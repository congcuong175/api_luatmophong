namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CAUHOI")]
    public partial class CAUHOI
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MACAUHOI { get; set; }

        [Column(TypeName = "text")]
        public string VIDEO { get; set; }

        public int? MALOAI { get; set; }

        public int? TRANGTHAI { get; set; }

        public int? COUNTWRONG { get; set; }

        [Column(TypeName = "ntext")]
        public string GOIY { get; set; }
    }
}
