namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DAPAN")]
    public partial class DAPAN
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MADAPAN { get; set; }

        public int? MACAUHOI { get; set; }

        public double? DIEMBD { get; set; }

        public double? DIEMKT { get; set; }
    }
}
