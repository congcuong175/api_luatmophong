using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace API_LuatMoPhong.Models
{
    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<BAILAM> BAILAMs { get; set; }
        public virtual DbSet<CAUHOI> CAUHOIs { get; set; }
        public virtual DbSet<DAPAN> DAPANs { get; set; }
        public virtual DbSet<DETHI> DETHIs { get; set; }
        public virtual DbSet<LAMTHEOLOAI> LAMTHEOLOAIs { get; set; }
        public virtual DbSet<LOAICAUHOI> LOAICAUHOIs { get; set; }
        public virtual DbSet<MEOCAU> MEOCAUs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CAUHOI>()
                .Property(e => e.VIDEO)
                .IsUnicode(false);

            modelBuilder.Entity<MEOCAU>()
                .Property(e => e.ANH)
                .IsUnicode(false);
        }

    }
}
