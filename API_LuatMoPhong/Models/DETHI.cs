namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DETHI")]
    public partial class DETHI
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MADE { get; set; }

        public int? THOIGIAN { get; set; }
    }
}
