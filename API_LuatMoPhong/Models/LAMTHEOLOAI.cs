namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LAMTHEOLOAI")]
    public partial class LAMTHEOLOAI
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MALAMTHEOLOAI { get; set; }

        public int? MALOAI { get; set; }

        public int? MACAUHOI { get; set; }

        public double? DIEMXN { get; set; }

        public int? KIEMTRA { get; set; }
    }
}
