namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LOAICAUHOI")]
    public partial class LOAICAUHOI
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MALOAI { get; set; }

        [Column(TypeName = "ntext")]
        public string TENLOAI { get; set; }

        [Column(TypeName = "ntext")]
        public string GHICHU { get; set; }

        [Column(TypeName = "ntext")]
        public string ICON { get; set; }
    }
}
