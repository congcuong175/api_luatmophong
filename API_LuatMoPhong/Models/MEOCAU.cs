namespace API_LuatMoPhong.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MEOCAU")]
    public partial class MEOCAU
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MAMEO { get; set; }

        public int? MACAUHOI { get; set; }

        [Column(TypeName = "text")]
        public string ANH { get; set; }

        [Column(TypeName = "ntext")]
        public string NOIDUNG { get; set; }
    }
}
